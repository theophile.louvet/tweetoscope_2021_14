import sys
import configparser
import threading
import json
import pickle
from utils import logger
from kafka import KafkaProducer
from kafka import KafkaConsumer

class ThreadProperties(threading.Thread):
    '''
        thread for receiving properties and managing them
    '''

    def __init__(self, config, logger):
        '''
            create producer/consumer, set up logger and config
        '''
        threading.Thread.__init__(self)

        self.producer = KafkaProducer(
            bootstrap_servers = config['kafka']['brokers'],                     # List of brokers passed from the command line
            value_serializer=lambda v: json.dumps(v).encode('utf-8'), # How to serialize the value to a binary buffer
            key_serializer=str.encode                                 # How to serialize the key
            )

        self.consumer = KafkaConsumer(config['topics']['properties'],                   # Topic name
            bootstrap_servers = config['kafka']['brokers'],                        # List of brokers passed from the command line
            value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
            key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
        )

        self.config = config
        self.logger = logger

    def run(self):

        global models

        cascades = {}
        for record in self.consumer:
            val = record.value
            key = str(record.key)
            name = str(val['cid']) + key
            if val['type'] =='parameters':
                #create prediction from the parameters
                message = self.prediction(key, val)
                message['T_obs'] = key
                cascades[name] = message
                #store prediction in the cascades dict
                self.logger.debug('Predicted cascade number %s (T_obs = %s)' %(val['cid'], key))
                if message['n_pred']>=int(self.config['cascade']['threshold']):
                    #if the prediction is superior to a thresold it is send to
                    # the alerts topic
                    self.producer.send(self.config['topics']['alerts'],
                                        key = key, value=val)
            elif val['type']=='size':
                self.logger.debug('Received size of cascade number %s (T_obs = %s)' %(val['cid'], key))
                if name in [*cascades]:
                    key, msg = self.send_sample(cascades[name], val)
                    #send to samples
                    self.send_stats(msg, val)
                    #send to stats
                    del cascades[name], msg
                else:
                    self.logger.warning("Couldn't find prediction for cascade number: %s (T_obs = %s)" %(val['cid'], key))
            else:
                self.logger.warning("Received incorrect message: type '%s'instead of 'size'!" %va['type'])

    def prediction(self, T_obs, message):
        '''
            predict the expected size of the cascade from the parameters
            models is a global variable as it is modified by the other thread
        '''
        global models

        beta, n_star, G1 = message['params']
        n = message['n_obs']

        w= models[T_obs].predict( [message['params']] )[0]

        Ntot = n + w*G1 / (1. - n_star)
        message['n_pred'] = Ntot
        message['T_obs']=T_obs
        return message

    def send_sample(self, msg, val):
        '''
        format the message to be sent to the samples topic
        '''

        msg['type'] = 'sample'
        msg['X'] = msg['params']
        del msg['params']
        beta, n_star, G1 = msg['X']
        w = (val['n_tot'] - msg['n_obs'])*(1-n_star)/G1
        msg['W'] = w
        del msg['n_supp']
        del msg['n_obs']
        key = msg['T_obs']
        self.producer.send(config['topics']['samples'], key=key, value=msg)
        return val, msg

    def send_stats(self, msg, val):
        ARE= abs(val['n_tot']-msg['n_pred'])/val['n_tot']
        del msg['X']
        del msg['W']
        msg['ARE'] = ARE
        msg['type'] = 'stat'
        self.producer.send(config['topics']['stats'], key='', value=msg)
        pass

class ThreadModel(threading.Thread):
    '''
        thread to receive the models, and update them in the global models var
    '''
    def __init__(self, config, logger):
        threading.Thread.__init__(self)

        self.consumer = KafkaConsumer(config['topics']['models'],                   # Topic name
            bootstrap_servers = config['kafka']['brokers'],                        # List of brokers passed from the command line
            key_deserializer= lambda v: v.decode(),                       # How to deserialize the key (if any)
            value_deserializer = lambda v: pickle.loads(v)
        )
        self.config = config
        self.logger = logger

    def run(self):
        #models is global so that a modification here is used in the process thread
        global models
        for record in self.consumer:
            models[record.key] = record.value
            self.logger.info('Received new model for observation window of %s' %record.key)

class Model():
    '''
        for initialization of the models dict
        the predict function return 1
    '''
    def predict(*args):
        return [1]

if __name__ == '__main__':

    config = configparser.ConfigParser()
    config.read(sys.argv[1])

    logger = logger.get_logger('Predictor', broker_list=config['kafka']['brokers'], debug=True)

    models = {}
    for window in config['times']['observation'].split(','):
        models[window] = Model()
    #create a model for the different observation windows

    prop = ThreadProperties(config, logger)
    mod = ThreadModel(config, logger)
    #create thread for the properties topic and models
    #they interact with each other through global variable models

    prop.start()
    mod.start()

    prop.join()
    mod.join()
