from collections import OrderedDict


class Cascade:
    """
    Cascade class
    """
    def __init__(self, init_tweet):
        """
        initialization of a cascade
        :param init_tweet: initial tweet of the cascade
        id : cascade id
        msg : init tweet msg
        tweets : list of tweets corresponding to the cascades => (t: timestamp, m: magnitude)
        """
        self.id = init_tweet.value["tweet_id"]
        self.msg = init_tweet.value['msg']
        self.tweets = [(init_tweet.value['t'], init_tweet.value['m'])]


class Processor:
    """
    Processor class
    """
    def __init__(self, obs_wins, init_tweet):
        """
        initialization of processor
        :param obs_wins: observation windows setup for partial
        :param init_tweet: initial tweets to setup first cascade of the source

        partial_cascades : dict of list of cascade id, with observation windows as keys,
        cascades : ordered dict of cascades object
        """
        self.partial_cascades, self.cascades = self.init_cascades(obs_wins, init_tweet)

    @staticmethod
    def init_cascades(obs_wins, init_tweet):
        """
        initialization of cascades dict and partial cascades list
        :param obs_wins: observation windows
        :param init_tweet: initial tweet of cascade
        :return: partial cascades dict and cascades dict
        """
        partial_cascades = {}
        cascades = OrderedDict()
        if init_tweet.value['type'] == "tweet":
            for win in obs_wins:
                partial_cascades[win] = [init_tweet.value["tweet_id"]]
            cascades[init_tweet.value["tweet_id"]] = Cascade(init_tweet)
        return partial_cascades, cascades

    def add_new_cascade(self, tweet):
        """
        Add new cascades and cascades id to dict of cascades and partial cascades
        :param tweet: new tweet sent from the tweet generator
        """
        self.cascades[tweet.value["tweet_id"]] = Cascade(tweet)
        for k in self.partial_cascades:
            self.partial_cascades[k].append(tweet.value["tweet_id"])

    @staticmethod
    def process_cascade_series(cascade, win):
        """
        Process message of cascade series
        :param cascade: partial cascade to be sent
        :param win: window operation selected
        :return: {msg}
        """
        return {'type': 'serie', 'cid': cascade.id, 'msg': cascade.msg,
                'T_obs': win, 'tweets': cascade.tweets}

    @staticmethod
    def process_cascade_properties(cascade):
        """
        Process message of cascade properties
        :param cascade: terminated cascade to be sent
        :return: {msg}
        """
        n_tot = len(cascade.tweets)
        t_end = cascade.tweets[-1][0]
        return {'type': 'size', 'cid': cascade.id, 'n_tot': n_tot, 't_end': t_end}

    def process_tweet(self, tweet, max_time_cascade, min_size_cascade):
        """
        Process current tweet, creating a new cascade if actual tweet is initial
        Looping in an ordered dict to check for terminated cascades
        Update corresponding cascade with tweet
        Checking for partial cascades to be sent
        :param tweet: current tweet
        :param max_time_cascade: parameters for terminated cascade
        :return: dict of terminated cascades and a list of partial cascades
        """
        terminated_cascades = {}
        partial_cascades = []

        if tweet.value['type'] == "tweet":
            self.add_new_cascade(tweet)
        else:
            for c in self.cascades.copy():
                if tweet.value['t'] - self.cascades[c].tweets[-1][0] > max_time_cascade:
                    if len(self.cascades[c].tweets) > min_size_cascade :
                        terminated_cascades[c] = self.process_cascade_properties(self.cascades[c])
                    del self.cascades[c]
                else:
                    if tweet.value["tweet_id"] in self.cascades:
                        self.cascades[tweet.value["tweet_id"]].tweets.append((tweet.value['t'], tweet.value['m']))
                        self.cascades.move_to_end(tweet.value["tweet_id"])
                    break  # ordered dict we can stop looping

            for win in self.partial_cascades:  # loop through observation windows
                for c_id in self.partial_cascades[win]:
                    if c_id in self.cascades:  # check if cascade not terminated
                        if tweet.value['t'] - self.cascades[c_id].tweets[0][0] > win:
                            if len(self.cascades[c_id].tweets) > min_size_cascade:
                                partial_cascades.append(self.process_cascade_series(self.cascades[c_id], win))
                            self.partial_cascades[win].remove(c_id)
                    else:
                        self.partial_cascades[win].remove(c_id)
        return terminated_cascades, partial_cascades
