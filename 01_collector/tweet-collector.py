import sys
import configparser
from params_collector import ParamsCollector
from collector import Collector
from utils import logger

if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read(sys.argv[1])
    logger = logger.get_logger('my-node', broker_list=config['kafka']['brokers'], debug=True)
    p = ParamsCollector(config['kafka']['brokers'], dict(config.items('topics')), dict(config.items('times')),
                        config['cascade']['min_cascade_size'])
    logger.info("Creating the collector")
    Coll = Collector(p, logger)
    logger.info("Collector created with parameters :")
    logger.info(Coll.params.print_params())
    logger.info(f'Collector running check topics {p.topic_out_series} and {p.topic_out_properties}')
    Coll.collecting()
