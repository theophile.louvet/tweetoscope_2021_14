import json
from processor_collector import Processor
from kafka import KafkaConsumer, KafkaProducer

#modification for video

class Collector:
    """
    Collector class, the collector is in charge of gathering retweets and grouping them into cascades.
    """
    def __init__(self, params, logger):
        """
        initialization of tweets collector from parameters file config
        collecting all tweets from generator
        processing tweets through processor
        send processed cascades to topics
        :param params: parameters class
        processors: dict of processors with tweet src for key
        consumer: kafka consumer to get tweets from topic tweets
        producer: kafka producer to send into topics
        """
        self.params = params
        self.logger = logger
        self.processors = {}
        self.consumer = KafkaConsumer(
            self.params.topic_in,
            group_id='collectors',
            bootstrap_servers=self.params.brokers,
            value_deserializer=lambda v: json.loads(v.decode('utf-8')), key_deserializer=lambda v: v.decode(),
        )
        self.producer = KafkaProducer(
            bootstrap_servers=self.params.brokers,
            value_serializer=lambda v: json.dumps(v).encode('utf-8'),
            key_serializer=str.encode
        )
        self.nb_process_tweet = 0

    def print_msg_cons_test(self):
        """test print msg from tweets topic"""
        print(10 * '*', f'Msg from {self.params.topic_in}', 10 * '*')
        for msg in self.consumer:
            print(f'key : {msg.key} and value {msg.value}')
            break

    def sens_msg_prod_test(self, topic_name):
        """test send msg to topic"""
        print(10 * '*', f'Sending msg to {topic_name}', 10 * '*')
        for _ in range(3):
            self.producer.send(topic_name, key="Test", value="This is a test")
        self.producer.flush()

    def send_terminated_cascades(self, terminated_cascades):
        """
        send terminated cascades with kafka producer to topic properties
        :param terminated_cascades: list of terminated cascades
        """
        if bool(terminated_cascades):
            for k in terminated_cascades:  # check if empty
                for win in self.params.obs_winds:
                    self.producer.send(self.params.topic_out_properties, key=str(win), value=terminated_cascades[k])
            self.producer.flush()
            self.logger.info(f"Terminated cascades ID {tuple(terminated_cascades.keys())} sent")

    def send_partial_cascades(self, partial_cascades):
        """
        send partial cascades with kafka producer to topic series
        :param partial_cascades: list of partial cascades
        """
        if partial_cascades:  # check if empty
            for k in partial_cascades:
                self.producer.send(self.params.topic_out_series, key="None", value=k)
            self.producer.flush()
            self.logger.info(f"NB partial cascades {len(partial_cascades)} sent")

    def collecting(self):
        """
        collecting tweets from topic tweets
        """
        for tweet in self.consumer:
            self.logger.info(f"Received tweet, key :  {tweet.key} with value : {tweet.value} from partition :{tweet.partition}")
            tweet_src = tweet.key
            if tweet_src in self.processors:
                terminated_cascades, partial_cascades = self.processors[tweet_src].process_tweet(tweet,
                                                                                                 self.params.max_time_cascade, self.params.min_size_cascade)
                self.send_terminated_cascades(terminated_cascades)
                self.send_partial_cascades(partial_cascades)
            else:
                self.processors[tweet_src] = Processor(self.params.obs_winds, tweet)
            self.logger.info(
                f"Collector consumer {self.consumer.config['client_id']} processed {self.nb_process_tweet} tweets")
            self.nb_process_tweet += 1

