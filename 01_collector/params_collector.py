class ParamsCollector:
    """
    Parameters collector class...
    """

    def __init__(self, brokers, topics, times, cascade):
        """
        initialization of collector parameters
        :param brokers: list of brokers
        :param topics: list of topics
        :param times: observation windows and max time terminated cascade
        :param cascade: cascade parameters
        """
        self.brokers = brokers
        self.topic_in = topics.get('tweets')
        self.topic_out_series = topics.get('series')
        self.topic_out_properties = topics.get('properties')
        self.obs_winds = [int(i) for i in times.get('observation').split(',')]
        self.max_time_cascade = int(times.get('terminated'))
        self.min_size_cascade = int(cascade)

    def print_params(self):
        return {
            "Kafka": {
                "Brokers": {self.brokers}
            },
            "Topics": {
                "Topic_in": {self.topic_in},
                "Topic_out_series": {self.topic_out_series},
                "Topic_out_properties": {self.topic_out_properties}
            },
            "Times": {
                "Observation_windows": {tuple(self.obs_winds)},
                "Max_time_cascade": {self.max_time_cascade}
            },
            "Cascade": {
                "Min_size_cascade": {self.min_size_cascade}
            }
        }
