import numpy as np
import scipy.optimize as optim
import configparser
import sys
import json
from kafka import KafkaProducer
from kafka import KafkaConsumer
from utils import logger

class HawkesEstimator:
    def __init__(self, cascade, prior=[ 0.02, 0.0002, 0.01, 0.001, -0.01]):
        """
        initialization of Hawkes estimator from a cascade serie
        compute the history and observation time from the cascade_serie
        define attributes of class:
            t = end of observation window
            history: array[:,2] with in [:,0] the timestamp of retweet and [:,1]
                    the magnitude of the retweet
            alpha: parameter for the distribution of the number of followers
            mu : parameter for the distribution of the number of followers
        """
        if cascade['type']!='serie':
            logger.warning('Passed %s as input of estimator' %cascade['type'])
            raise ValueError

        self.cid = cascade["cid"]
        self.msg = cascade["msg"]
        self.t = cascade["T_obs"]
        self.history = np.array([list(a) for a in cascade['tweets']])
        self.alpha = 2.41
        self.mu = 1
        self.prior = prior

    def estimate(self):
        """
        Compute the parameters (p, beta, G1) of the cascade with the MAP estimator,
        from these parameters calculate the predicted number of retweets
        return the estimated size of the cascade and the parameters found
        """

        self.history[:, 0] -= self.history[0, 0]

        #adjusting so that the cascade starts at T=0

        try:
            LL, params= self._compute_MAP(self.history, self.t)
            self.N_tot = self._predict(params[0], params[1])
            logger.debug('Estimated parameters for cascade number %s and observation window of %ds' %(self.cid, self.t))
        except:
             logger.warning('Estimation failed for cascade number %s with %d observations'
                         %(self.cid, len(self.history)))
             self.params=[-1,-1, -1]
             self.N_tot = -1
             pass
        pass

    def _compute_MAP(self, history, t):
        """
        Returns the pair of the estimated logdensity of a posteriori and parameters (as a numpy array)

        history      -- (n,2) numpy array containing marked time points (t_i,m_i)
        t            -- current time (i.e end of observation window)

        other parameters are kept by default
        """
        max_n_star = 1

        # Compute prior moments
        mu_p, mu_beta, sig_p, sig_beta, corr = self.prior

        sample_mean = np.array([mu_p, mu_beta])
        cov_p_beta = corr * sig_p * sig_beta
        Q = np.array([[sig_p ** 2, cov_p_beta], [cov_p_beta, sig_beta **2]])

        # Apply method of moments
        cov_prior = np.log(Q / sample_mean.reshape((-1,1)) / sample_mean.reshape((1,-1)) + 1)
        mean_prior = np.log(sample_mean) - np.diag(cov_prior) / 2.

        # Compute the covariance inverse (precision matrix) once for all
        inv_cov_prior = np.asmatrix(cov_prior).I

        # Define the target function to minimize as minus the log of the a posteriori density
        def target(params):

            log_params = np.log(params)

            if np.any(np.isnan(log_params)):
                return np.inf
            else:
                dparams = np.asmatrix(log_params - mean_prior)
                prior_term = float(- 1/2 * dparams * inv_cov_prior * dparams.T)
                logLL = self._loglikelihood(params, history, t)
                return - (prior_term + logLL)

        EM = self.mu * (self.alpha - 1) / (self.alpha - 2)
        eps = 1.E-8

        # Set realistic bounds on p and beta
        p_min, p_max       = eps, max_n_star/EM - eps
        beta_min, beta_max = 1/(3600. * 24 * 10), 1/(60. * 1)

        # Define the bounds on p (first column) and beta (second column)
        bounds = optim.Bounds(
            np.array([p_min, beta_min]),
            np.array([p_max, beta_max])
        )

        # Run the optimization
        res = optim.minimize(
            target, sample_mean,
            method='Powell',
            bounds=bounds,
            options={'xtol': 1e-8, 'disp': False}
        )
        # Returns the loglikelihood and found parameters
        return(-res.fun, res.x)

    def _loglikelihood(self, params, history, t):
        """
        Returns the loglikelihood of a Hawkes process with exponential kernel
        computed with a linear time complexity

        params   -- parameter tuple (p,beta) of the Hawkes process
        history  -- (n,2) numpy array containing marked time points (t_i,m_i)
        t        -- current time (i.e end of observation window)
        """

        p,beta = params

        if p <= 0 or p >= 1 or beta <= 0.: return -np.inf

        n = len(history)
        tis = history[:,0]
        mis = history[:,1]

        LL = (n-1) * np.log(p * beta)
        logA = -np.inf
        prev_ti, prev_mi = history[0]

        i = 0
        for ti,mi in history[1:]:
            if(prev_mi + np.exp(logA) <= 0):
                print("Bad value", prev_mi + np.exp(logA))

            logA = np.log(prev_mi + np.exp(logA)) - beta * (ti - prev_ti)
            LL += logA
            prev_ti,prev_mi = ti,mi
            i += 1

        logA = np.log(prev_mi + np.exp(logA)) - beta * (t - prev_ti)
        LL -= p * (np.sum(mis) - np.exp(logA))

        return LL

    def _predict(self, p, beta):
        """
        Returns the expected total numbers of points for a set of time points
        Also add G1 to the parameters list

        params   -- parameter tuple (p,beta) of the Hawkes process
        history  -- (n,2) numpy array containing marked time points (t_i,m_i)
        alpha    -- power parameter of the power-law mark distribution
        mu       -- min value parameter of the power-law mark distribution
        t        -- current time (i.e end of observation window)
        """

        tis = self.history[:,0]
        EM = self.mu * (self.alpha - 1) / (self.alpha - 2)
        n_star = p * EM
        if n_star >= 1:
            self.params = [-1, -1, -1]
            logger.warning(f"Branching factor {n_star:.2f} greater than one")
            return -1
        n = len(self.history)

        I = self.history[:,0] < self.t
        tis = self.history[I,0]
        mis = self.history[I,1]
        G1 = p * np.sum(mis * np.exp(-beta * (self.t - tis)))
        Ntot = n + G1 / (1. - n_star)

        self.params = [beta, n_star, G1]
        return int(Ntot)

    def format(self):
        """
        format the informations as the standardized output format to send to the
        cascade properties topic
        return the key and values to send
        """
        key = self.t

        value = {
                'type' : 'parameters',
                'cid': self.cid,
                'msg': self.msg,
                'n_obs': len(self.history[:,0]),
                'n_supp' : self.N_tot,
                'params': self.params,
                }
        return key, value

if __name__ == '__main__':

    config = configparser.ConfigParser()
    config.read(sys.argv[1])

    logger = logger.get_logger('Estimator', broker_list=config['kafka']['brokers'], debug=True)

    producer = KafkaProducer(
        bootstrap_servers = config['kafka']['brokers'],                     # List of brokers passed from the command line
        value_serializer=lambda v: json.dumps(v).encode('utf-8'), # How to serialize the value to a binary buffer
        key_serializer=str.encode                                 # How to serialize the key
        )

    consumer = KafkaConsumer(config['topics']['series'],                   # Topic name
        bootstrap_servers = config['kafka']['brokers'],                        # List of brokers passed from the command line
        value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize the value from a binary buffergi
        key_deserializer= lambda v: v                  # How to deserialize the key (if any)
    )

    for msg in consumer:
        estimator = HawkesEstimator(msg.value)
        estimator.estimate()
        key, val = estimator.format()
        #predict, and get formated output
        if val['n_supp']!=-1:
            producer.send(config['topics']['properties'], key = str(key), value=val)
        del estimator
