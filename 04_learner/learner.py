from kafka import KafkaConsumer, KafkaProducer
import json
import configparser
import sys
from sklearn.ensemble import RandomForestRegressor
import pickle
from utils import logger


class Learner:
    """
    The learner is in charge of collecting training samples and learn random forest models for various observation windows.
    """

    def __init__(self, params, logger):
        """
              initialization of the Learner
              :param params: parameters class
        """
        self.params = params
        self.logger = logger
        self.consumer = KafkaConsumer(
            self.params['topic_in'],
            bootstrap_servers=self.params['broker'],
            value_deserializer=lambda v: json.loads(v.decode('utf-8')), key_deserializer=lambda v: v.decode(),
        )
        self.producer = KafkaProducer(
            bootstrap_servers=self.params['broker'],
            ## no need to serialize, we send à pickle format of random forest
            key_serializer=str.encode
        )
        self.datasets = {}
        self.run = False
        self.min_size_dataset = 100
        self.prev_dataset_size = {}

    def check_datasets_size(self):
        """
                   Call learning at each size step dataset passed
        """
        for d in self.datasets:
            if d in self.prev_dataset_size:
                if len(self.datasets[d]['X']) > 10 and len(self.datasets[d]['X']) >= 2 * self.prev_dataset_size[d]:
                    self.logger.info(
                        f"Datasets for obs {d} : previous size : {self.prev_dataset_size[d]}, new size : {len(self.datasets[d]['X'])}")
                    self.prev_dataset_size[d] = (len(self.datasets[d]['X']))
                    self.learning(d)

    def update_size_dataset(self):
        for d in self.datasets:
            self.prev_dataset_size[d] = (len(self.datasets[d]['X']))

    def building_datasets(self, sample):
        """
            Buildind/adding sample for datasets of each obs windows
        """
        if sample.key in self.datasets:
            self.datasets[sample.key]["X"].append(sample.value["X"])
            self.datasets[sample.key]["W"].append(sample.value['W'])
            self.check_datasets_size()
        else:
            self.datasets[sample.key] = {"X": [sample.value["X"]], "W": [sample.value['W']]}
            self.prev_dataset_size[sample.key] = 1

    def collect_samples(self):
        for msg in self.consumer:
            if msg.value["type"] == "sample":
                self.building_datasets(msg)

    def create_model_fit(self, dataset):
        """
            Random Forest model creation
        """
        X, y = dataset["X"], dataset["W"]
        regr = RandomForestRegressor(max_depth=2, random_state=0)
        regr.fit(X, y)
        self.logger.info(f"Random forest fit for {dataset}")
        return regr

    def send_models(self, model, win):
        m = pickle.dumps(model)
        self.producer.send(self.params["topic_out"], key=win, value=m)
        self.producer.flush()

    def learning(self, win):
        regr = self.create_model_fit(self.datasets[win])
        self.send_models(regr, win)


if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read(sys.argv[1])
    params = dict()
    logger = logger.get_logger('my-node', broker_list=config['kafka']['brokers'], debug=True)
    params["broker"] = config['kafka']['brokers']
    params["topic_in"] = config['topics']['samples']
    params["topic_out"] = config['topics']['models']
    L = Learner(params, logger)
    L.collect_samples()
