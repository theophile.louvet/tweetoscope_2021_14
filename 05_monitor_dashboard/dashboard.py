from kafka import KafkaConsumer
import configparser
import sys
import json

if __name__ == '__main__':

    config = configparser.ConfigParser()
    config.read(sys.argv[1])

    consumer = KafkaConsumer(config['topics']['alerts'],                   # Topic name
        bootstrap_servers = config['kafka']['brokers'],                        # List of brokers passed from the command line
        value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
        key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
    )

    for msg in consumer:
        print("ALERT: message with id %s is predicted to have a total size of %s (message: %s, observation window: %ss)"
        %(msg.value['cid'], int(msg.value['n_pred']), msg.value['msg'], msg.value['T_obs']))
