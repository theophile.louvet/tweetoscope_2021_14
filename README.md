# Tweetoscope n°14

[![Build Status](https://gitlab-student.centralesupelec.fr/theophile.louvet/tweetoscope_2021_14/badges/master/pipeline.svg)]()

## Authors :
- [Alain Malige](mailto:alain.malige@student-cs.fr>)
- [Théophile Louvet](mailto:theophile.louvet@student-cs.fr>)
- [Tiago Teixeira ](mailto:tiago.teixeira@student-cs.fr>)
## Link to the demo :
> https://youtu.be/mfnvbEVcOfM


## Description :

This project was developed as part of Virginie Galtier's Software Application Engineering course in SDI at CentraleSupélec. The main objective of the project was to apply the new knowledge and skills learned in the course. 

![Meme](img/meme.png)


The tweetoscope aims at predicting the popularity of a tweet using a statistical model, the Hawkes process. 

![Hawkes process](img/hwksproc.png)

> <em>Image from Frederic Pennerath Course </em>

## Features

- A tweet generator which simulates the arrival in real time of tweets.
- A tweet collector that is in charge of gathering retweets and grouping them into cascades.
- The Hawkes estimator is in charge of estimating parameters of a hawkes process.
- The predictor is in charge of predicting the popularity of a tweet.
- The learner is in charge of collecting training samples and learn random forest models for various observation windows.
- The dashboard is a tool that dynamically displays the hot-topics.
- The monitor is in charge of monitoring the performance of the system.


![Archi project](img/Archi.svg)
> <em>Image from the [Tweetoscope subject] </em>
> 
## Tech

For this project we used : 

- [C++] - A wonderful programming language 
- [Python] - Another wonderful programming language
- [Kafka] - An open-source distributed event streaming platform
- [Docker] - An open platform for developping, shipping and running applications
- [Gitlab CI/CD] - Is a tool for software development using the continous methodologies
- [Kubernetes] - An open-source container orchestration platform





## Installation

### Docker Compose

We got some troubles with Kafka and docker compose, our services couldn't find available brokers.
Beside the fact that we ran successful tests with producers and consumers, we faced the issue of "Leader Not Available" that we have not been able to resolve.
```sh
cd ..
sudo apt install docker-compose
cd docker_compose
docker-compose -f docker-compose-middleware.yml
docker-compose -f docker-compose-services.yml up
```

### Kubernetes cluster

```sh
# cpu connection
ssh cpusdi1_XX@phome.metz.supelec.fr
<password>
ssh <assigned host>

# copy files to cluster
scp yaml_files/zookeeper-and-kafka.yml cpusdi1_XX@phome.metz.supelec.fr:/usr/users/cpusdi1/cpusdi1_XX
scp yaml_files/tweetoscope-deploy_cpusdi1_40.yml cpusdi1_XX@phome.metz.supelec.fr:/usr/users/cpusdi1/cpusdi1_XX

# run project
kubectl -n cpusdi1-XX-ns apply -f zookeeper-and-kafka.yml
kubectl -n cpusdi1-XX-ns get pods 
# wait for kafka
kubectl -n cpusdi1-XX-NS apply -f tweetoscope-deploy_cpusdi1_40.yml
```


## License

MIT

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [Kubernetes]: <https://kubernetes.io/fr/>
   [Gitlab CI/CD]: <https://docs.gitlab.com/ee/ci/>
   [Docker]: <https://www.docker.com/>
   [Kafka]: <https://kafka.apache.org/>
   [C++]: <https://en.cppreference.com/w/>
   [Python]: <https://www.python.org/>
   [Python]: <https://www.python.org/>
   [Tweetoscope subject]: <https://sdi.metz.centralesupelec.fr/spip.php?article25/>
